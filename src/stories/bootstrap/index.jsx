import React from 'react'
import { storiesOf } from '@storybook/react'

import bootswatchHtml from './bootswatch.html'

const chapter = storiesOf('Bootstrap', module)

chapter.add('Bootswatch', () => <div dangerouslySetInnerHTML={{__html:bootswatchHtml}}/>)