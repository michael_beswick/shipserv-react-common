import React from "react"

import "../../boostrap/bootstrap.scss"
import "./index.scss"

import {Container, Row, Col} from "reactstrap"
import logo from "./logo.svg"

const Footer = () => (
    <footer>
        <Container>
            <Row className="show-grid">
                <Col xs={12} md={8}>
                    <img src={logo} alt="ShipServ logo" className="logo"/>
                </Col>
                <Col xs={6} md={4} className="text-right">
                    <ul className="social-links">
                        <li>
                            <a
                                href="https://www.facebook.com/pages/ShipServ-Maritime-Trading-Network/209142614873"
                                target="blank"
                                rel="noopener noreferrer"></a>
                        </li>
                        <li>
                            <a
                                href="https://twitter.com/ShipServ"
                                target="_blank"
                                rel="noopener noreferrer"></a>
                        </li>
                        <li>
                            <a
                                href="https://www.linkedin.com/company/shipserv"
                                target="_blank"
                                rel="noopener noreferrer"></a>
                        </li>
                    </ul>
                </Col>
            </Row>
            <Row>
                <Col xs={6} md={4}>
                    <h2>About us</h2>
                    <ul>
                        <li>
                            <a href="/info/about-us">About ShipServ</a>
                        </li>
                        <li>
                            <a href="/info/contact-us">Contact Us</a>
                        </li>
                        <li>
                            <a href="/info/pages-for-suppliers/webstore">Store</a>
                        </li>
                        <li>
                            <a href="/info/contact-us">Support</a>
                        </li>
                        <li>
                            <a href="/info/save-time-and-money-with-tradenet">
                                Buyer Solutions
                            </a>
                        </li>
                        <li>
                            <a href="/info/pages-for-suppliers">Supplier Solutions</a>
                        </li>
                    </ul>
                </Col>
                <Col xs={6} md={4}>
                    <h2>Search Suppliers</h2>
                    <ul>
                        <li>
                            <a href="/supplier/category">By category</a>
                        </li>
                        <li>
                            <a href="/supplier/brand">By brand</a>
                        </li>
                        <li>
                            <a href="/supplier/country">By country</a>
                        </li>
                        <li>
                            <a href="/supplier/browse">Supplier A-Z</a>
                        </li>
                    </ul>
                </Col>
                <Col xsHidden md={4}>
                    <h2>Important Legal Matters</h2>
                    <ul>
                        <li>
                            <a href="/info/terms-of-use">Pages - Terms of Use</a>
                        </li>
                        <li>
                            <a href="/info/service-conditions">Pages - Service Conditions</a>
                        </li>
                        <li>
                            <a href="/info/service-guidelines">Pages - Service Guidelines</a>
                        </li>
                        <li>
                            <a href="/info/privacy-policy">Privacy Policy</a>
                        </li>
                        <li>
                            <a href="/info/legal-notes/cookie-policy">Cookie Policy</a>
                        </li>
                        <li>
                            <a href="/info/legal-notes">Other ShipServ conditions</a>
                        </li>
                        <li>
                            <a href="/info/shipserv-membership-and-definitions">
                                ShipServ Membership terms
                            </a>
                        </li>
                    </ul>
                </Col>
            </Row>
            <Row className="text-center">
                <Col xs={12}>
                    <p>
                        <em>Copyright © 2017 ShipServ Limited. All rights reserved.</em>
                    </p>
                </Col>
            </Row>
        </Container>
    </footer>
)

export default Footer
