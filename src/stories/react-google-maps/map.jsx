import React from 'react'

import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker,
} from "react-google-maps"

const MapWithAMarker = withScriptjs(withGoogleMap(props =>
    <GoogleMap defaultZoom={10} defaultCenter={{ lat: 51.494837, lng: -0.276049 }}>
        <Marker position={{ lat: 51.494837, lng: -0.276049 }} />
    </GoogleMap>
))

const example = (props) => {
    return (<MapWithAMarker
        googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyAu6Y3JdqLfW1VL6QYzQcVcmNektXWJHFU"
        loadingElement={<div style={{ height: '100%' }} />}
        containerElement={<div style={{ height: '200px' }} />}
        mapElement={<div style={{ height: '100%' }} />}
    />
    )
}

export default function (stories) {
    stories.add('Basic Map', example)
}