import React from 'react'
import { storiesOf } from '@storybook/react'

import FontAwesome from 'react-fontawesome'
import jquery from 'jquery'

global.$ = jquery;
global.jQuery = jquery;

import '../../../shipserv-pages/navigation'
import '../../../shipserv-pages/styles.scss'

import headerHtml from './header.html'
import footerHtml from './footer.html'
import layoutHtml from './layout.html'

const chapter = storiesOf('Pages HTML', module)

chapter.add('Header', () => <div dangerouslySetInnerHTML={{__html:headerHtml}}/>)
chapter.add('Footer', () => <div dangerouslySetInnerHTML={{__html:footerHtml}}/>)
chapter.add('Layout', () => <div dangerouslySetInnerHTML={{__html:layoutHtml}}/>)