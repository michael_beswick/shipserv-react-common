const path = require('path')

module.exports = {
    module: {
        rules: [{
                test: /\.scss$/,
                //loaders: ['style-loader', 'css-loader', 'sass-loader'],
                include: path.resolve(__dirname, '../'),
                use: [{
                    loader: "style-loader"
                }, {
                    loader: "css-loader",
                    options: {
                        sourceMap: true
                    }
                }, {
                    loader: "sass-loader",
                    options: {
                        sourceMap: true
                    }
                }]
            },
            {
                test: /\.html$/,
                loaders: ['html-loader'],
                include: path.resolve(__dirname, '../')
            },
            {
                test: /\.(svg)$/,
                loader: 'svg-loader'
            }
        ]
    }
}