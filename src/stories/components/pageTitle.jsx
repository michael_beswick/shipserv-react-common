import React from 'react'
import PageTitle from '../../../components/pageTitle'

export default function (stories) {
    stories.add('PageTitle', () => <PageTitle>Title Text</PageTitle>)
}
