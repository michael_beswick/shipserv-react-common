import { Highcharts } from 'react-highcharts';

Highcharts.setOptions({
    url: null,
    chart: {
        //height: 320,
        animation: false,
        reflow: true,
        spacing: [10, 0, 0, 0]
    },
    title: {
        text: null
    },
    subTitle: {
        text: null
    },
    xAxis: {
        lineWidth: 1,
        lineColor: '#9BA7BA',
        tickColor: '#9BA7BA',
        tickLength: 5,
        gridLineWidth: 0,
        title: {
            style: {
                fontSize: '16px'
            }
        },
        labels: {
            style: {
                fontSize: '14px'
            }
        }
    },
    yAxis: {
        lineWidth: 1,
        lineColor: '#9BA7BA',
        tickColor: '#9BA7BA',
        tickLength: 5,
        gridLineWidth: 0,
        title: {
            style: {
                fontSize: '16px'
            }
        },
        labels: {
            style: {
                fontSize: '12px'
            }
        }
    },
    tooltip: {
        backgroundColor: "rgba(255,255,255,1)",
    },
    colors: [
        '#3494CD',
        '#9171B1',
        '#829621',
        '#F2AF69',
        '#EA8787',
        '#7989A0',
        '#4ECCCB',
        '#D5CD6C',
        '#99D3E7',
        '#C0A1DC'
    ],
    plotOptions: {
        line: {
            marker: {
                symbol: 'square'
            },
            line: {
            },
            series: {
                connectNulls: true,
                clip: false
            }
        },
        column: {
            pointPadding: 0.1,
            groupPadding: 0,
            borderWidth: 0,
            shadow: false
        },
        pie: {
            size: 100,
            borderWidth: 0,
            allowPointSelect: true,
            cursor: 'pointer',
            dataLabels: {
                enabled: true,
                connectorColor: '#000',
                softConnector: true,
                crop: false,
                style: {
                    fontSize: '14px',
                    fontWeight: 'normal'
                }
            },
            showInLegend: true,
            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span>{point.name}</span>: <br> <b>{point.y:.0f}%</b> of total<br/>',
                shared: false
            }
        },
        series: {
            events: {
                legendItemClick: function () {
                    return false; // <== returning false will cancel the default action
                }
            },
            point: {
                events: {
                    legendItemClick: function () {
                        return false; // <== returning false will cancel the default action
                    }
                }
            }
        }
    },
    responsive: {
        rules: [
            {
                condition: {
                    minWidth: 350
                },
                chartOptions: {
                    plotOptions: {
                        pie: {
                            size: 250
                        }
                    }
                }
            }, {
                condition: {
                    minWidth: 600
                },
                chartOptions: {
                    plotOptions: {
                        pie: {
                            size: 250
                        }
                    }
                }
            }
        ]
    },
    legend: {
        symbolHeight: 12,
        symbolWidth: 12,
        symbolRadius: 0,
        itemStyle: {
            fontSize: '14px',
            fontWeight: 'normal',
            cursor: 'pointer'
        }
    },
    series: [],
    credits: {
        enabled: false
    },
    lang: {
        thousandsSep: ','
    }
});