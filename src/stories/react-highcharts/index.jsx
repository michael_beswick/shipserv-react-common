import { storiesOf } from '@storybook/react'

const chapter = storiesOf('React Highcharts', module)

require('./line').default(chapter)
require('./pie').default(chapter)
require('./column').default(chapter)
require('./area').default(chapter)
require('./scatterPlot').default(chapter)
require('./percentageArea').default(chapter)

