import React from 'react'
import PropTypes from 'prop-types'

import './style.scss'

const PageTitle = (props) => (
  <h1 className="page-title">
    {props.children}
  </h1>
)

PageTitle.propTypes = {}

export default PageTitle;
