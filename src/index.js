import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import 'font-awsome/scss/font-swome.scss'

ReactDOM.render(<App />, document.getElementById('root'))

registerServiceWorker()
