import { storiesOf } from '@storybook/react'

const chapter = storiesOf('ShipServ Components', module)

require('./header').default(chapter)
require('./footer').default(chapter)
require('./pageTitle').default(chapter)
